import java.io.*;
import java.util.*;

class WordCount	{

	public static void main(String args[])	{
		Scanner scan = new Scanner(System.in);
		try	{
			Map<String, Integer> map = new TreeMap<String, Integer>();
			File file = new File ("data.txt");
			scan = new Scanner(file);
			while(scan.hasNextLine())	{
				String line = scan.nextLine();
				//System.out.println(line);
				String[] tokens = line.split("[ .-?,\"]");

				for(int i = 0 ;i< tokens.length ;i++)
				{
			
					if(!map.containsKey(tokens[i]))
						map.put(tokens[i],1);
					else
						map.put(tokens[i],1+map.get(tokens[i]));
						
				}

			}

			//	System.out.println(map);

			for (String name: map.keySet())	{

            	String key =name;
            	int value = map.get(name);  
            	System.out.println(key + " " + value);  


			} 


		}	catch(Exception e)	{
			e.printStackTrace();
		}
	}
}