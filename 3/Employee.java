
import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

public class Employee {

 public static void main(String[] args) {
  try {
   // connect to mongoDB, IP and port number
   MongoClient mongoClient = new MongoClient("localhost", 27017);
 
   // get database from MongoDB,
   // if database doesn't exists, mongoDB will create it automatically
   DB db = mongoClient.getDB("EmployeeDB");
 
   // if collection doesn't exists, mongoDB will create it automatically
   DBCollection collection = db.getCollection("Employee");
 
   // create a document to store key and value
   BasicDBObject document = new BasicDBObject();
   document.put("id", 1000);
   document.put("name", "Sumit Bansal");
   document.put("phone", "8447502000");

 
   collection.insert(document);
 
  BasicDBObject document1 = new BasicDBObject();
   document1.put("id", 1001);
   document1.put("name", "Shubham");
   document1.put("phone", "8000000000");

 
   collection.insert(document1);
 
   	BasicDBObject document2 = new BasicDBObject();
   document2.put("id", 1002);
   document2.put("name", "Xyz");
   document2.put("phone", "1234567890");

 
   
   collection.insert(document2);

   // search query
   BasicDBObject searchQuery = new BasicDBObject();
   searchQuery.put("id", 1000);
 
   // query it
   DBCursor cursor = collection.find(searchQuery);
 	System.out.println("\nOn The Basis Of ID :");
   // loop over the cursor and display the retrieved result
   while (cursor.hasNext()) {
    System.out.println(cursor.next());
   }
 
 BasicDBObject searchQuery1 = new BasicDBObject();
   searchQuery1.put("name", "Xyz");
 
   // query it
   DBCursor cursor1 = collection.find(searchQuery1);
 	System.out.println("\nOn The Basis Of Name :");
   // loop over the cursor and display the retrieved result
   while (cursor1.hasNext()) {
    System.out.println(cursor1.next());
   }
 

   System.out.println("Done");
 
  } catch (Exception e) {
    System.out.println(e.getClass().getName());
   e.printStackTrace();
  }
 
 }

}
