import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Json2XML {

  
  public static void main(String[] args) throws JSONException {
    String jsonStr = "{ employee : { age:32, name : ABC DEF,  married:true}}";
    JSONObject jsonObj = new JSONObject(jsonStr);
    System.out.println(XML.toString(jsonObj));
  }

}